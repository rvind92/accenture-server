const db = require('./setup/setup-db')
const express = require('express')
const app = require('./setup/setup-express')(express())
const passport = require('passport')

app.get('/login', (request, response) => {
  if(request.isAuthenticated()) return response.redirect('/')
  response.render('login-page.ejs')
})

app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email'] }))

app.get('/auth/facebook/callback', (request, response, next) => {
  passport.authenticate('facebook', (err, user, info) => {
    if (err) {
      console.error({
        message: 'Facebook auth error',
        error: err
      })
      return response.redirect('/login')
    }
    if (!user) return response.redirect('/login')
    request.logIn(user, err => {
      if (err) return next(err)
      return response.redirect('/')
    })
  })(request, response, next)
})

app.use('/api/booking', require('./routes/booking'))
app.use('/api/payment', require('./routes/payment'))
app.use('/api/places', require('./routes/places'))

app.use((request, response, next) => {
  if (request.isAuthenticated()) return next()

  return response.redirect('/login')
})

app.use(express.static('build'))
app.get('/*', (request, response) => {
  response.sendFile('index.html', { root: './build' })
})

db.sequelize.sync({ force: false })
	.then(() => {
    const server = app.listen(app.get('port'), () => {
      console.log(`App running at ${server.address().port}!`)
    })
	})
	.catch((err) => {
		console.error('Unable to connect to the database: ', err)
	})
