import React, { Component } from 'react'
import { Link } from "react-router-dom"

class Header extends Component {
  state = {
    collapse: {
      topClass: 'bg-dark collapse',
      buttonClass: 'navbar-toggler collapsed',
      expanded: 'false'
    },
    expand: {
      topClass: 'bg-dark collapse show',
      buttonClass: 'navbar-toggler',
      expanded: 'true'
    },
    currentState: {
      topClass: 'bg-dark collapse',
      buttonClass: 'navbar-toggler collapsed',
      expanded: 'false'
    },
    collapsed: true,
  }

  toggleCollapse = e => {
    const { collapsed, expand, collapse } = this.state

    if(collapsed) {
      this.setState({
        currentState: {
          topClass: 'bg-dark collapse show',
          buttonClass: 'navbar-toggler',
          expanded: 'true'
        }, collapsed: !collapsed
      })
    } else {
      this.setState({
        currentState: {
          topClass: 'bg-dark collapse',
          buttonClass: 'navbar-toggler collapsed',
          expanded: 'false'
        }, collapsed: !collapsed
      })
    }
  }

  render() {
    const { currentState } = this.state
    return (
      <header>
        <div className={currentState.topClass} id="navbarHeader">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 col-md-7 py-4">
              </div>
              <div className="col-sm-4 offset-md-1 py-4">
                <h4 className="text-white">Menu</h4>
                <ul className="list-unstyled">
                  <li className="text-white" onClick={this.toggleCollapse}><Link to="/">Home</Link></li>
                  <li className="text-white" onClick={this.toggleCollapse}><Link to="/bookings">Bookings</Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="navbar navbar-dark bg-dark box-shadow">
          <div className="container d-flex justify-content-between">
            <a href="#" className="navbar-brand d-flex align-items-center">
              <img id='top-logo' src="http://www.logospng.com/images/120/timeline-accenture-120714.ashx" alt="asfsd" style={{ maxWidth: '5%', display: 'block' }} />
              <strong style={{ color: 'white' }}>Accenture Hotel</strong>
            </a>
            <button
              className={currentState.buttonClass}
              type="button"
              data-toggle="collapse"
              data-target="#navbarHeader"
              aria-controls="navbarHeader"
              aria-expanded={currentState.expanded}
              aria-label="Toggle navigation"
              onClick={this.toggleCollapse}>
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>
      </header>
    )
  }
}

export default Header
