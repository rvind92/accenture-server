const express = require('express')
const router = express.Router()
const axios = require('axios')
const db = require('../setup/setup-db')
const API_KEY = process.env.MAPS_API_KEY || 'AIzaSyCT2ZLMkZOHk_Vy8PR4w-7-Im-Z2t_Fi6E'

router.post('/', async (request, response) => {
  const { lat, lng } = request.body
  try {
    const PLACE_URL = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${lng}&radius=5000&type=museum&key=${API_KEY}`
    const { data } = await axios.get(PLACE_URL)
    response.status(200).json(data)
  } catch (e) {
    const err = {
      message: 'Unable to search for location',
      error: e
    }
    console.error(err)
    response.status(400).json(err)
  }
})

module.exports = router
