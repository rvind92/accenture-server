const AWS = require('../setup/setup-aws')

const ses = new AWS.SES({ apiVersion: '2010-12-01', region: 'us-east-1' })

const send = (email, id, success) => {
  const title = success ? 'Your order is successful!' : 'Your order has failed!'
  const body = success ? `Thank you for staying with us. Your booking ID is ${id}.` : 'Please try again.'

  const params = {
    Destination: {
      ToAddresses: [ email ]
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data:
            `<html>
              <body>
                <h1>${title}</h1>
                <p>${body}</p>
              </body>
             </html>`
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Accenture Hotel Booking Acknowledgment'
      }
    },
    Source: email
  }

  const sendEmail = ses.sendEmail(params).promise()

  sendEmail
    .then(data => console.log('email submitted to SES', data))
    .catch(error => console.log(error))
}

module.exports = send
