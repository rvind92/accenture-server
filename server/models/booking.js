module.exports = (sequelize, DataTypes) => {
	return sequelize.define('booking', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
			defaultValue: DataTypes.UUIDV4,
    },
		email: {
			type: DataTypes.STRING,
			allowNull: false
		},
    pax: {
      type: DataTypes.STRING,
      allowNull: false
    },
		status: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false
		},
    package: {
      type: DataTypes.STRING,
      allowNull: false
    },
		hotel: {
			type: DataTypes.STRING,
      allowNull: false
		}
	})
}
