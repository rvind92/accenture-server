import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getBookings } from '../../actions'

class Bookings extends Component {
  state = {
    bookings: [],
    list: []
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return { bookings: nextProps.bookings }
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.list.length === 0) {
      this.renderBookings()
    }
  }

  componentDidMount() {
    this.props.getBookings()
  }

  renderBookings = () => {
    const { bookings } = this.state

    const list = bookings.map((b, i) => (
      <a key={i} className={`list-group-item list-group-item-action ${b.status ? 'list-group-item-success' : 'list-group-item-danger'}`}>
        <div className='d-flex w-100 justify-content-between'>
          <h5 className='mb-1'>{b.hotel}</h5>
          <small>{b.createdAt}</small>
        </div>
        <p className='mb-1'>Package: {b.package}</p>
        <div><small>Pax: {b.pax}</small></div>
        <div><small>Status: {b.status}</small></div>
      </a>
    ))

    this.setState({ list })
  }

  render() {
    return (
      <div className='list-group' style={{ margin: '10px' }}>
        {this.state.list}
      </div>
    )
  }
}

const mapStateToProps = ({ bookings }) => ({ bookings })

export default connect(mapStateToProps, { getBookings })(Bookings)
