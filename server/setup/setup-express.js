const express = require('express')
const app = express()
const session = require('express-session')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cookieSession = require('cookie-session')
const passport = require('passport')
const cors = require('cors')
const PORT = process.env.PORT || 3000

module.exports = app => {
  app.set('views', `/${__dirname}/../views`)
  app.set('view engine', 'ejs')
  app.set('port', PORT)
  app.use(cors())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(cookieParser())
  app.use(cookieSession({
    secret: 'fixme2345',
    cookie: {
      maxAge: 30 * 60 * 1000
    }
  }))
  app.use(passport.initialize())
  app.use(passport.session())

  require('./setup-passport')(passport)

  return app
}
