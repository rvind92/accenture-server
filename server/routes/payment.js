const express = require('express')
const router = express.Router()
const db = require('../setup/setup-db')
const sendSes = require('../utils/ses-send')
const errorResponse = require('../utils/error-handler')

router.post('/', async (request, response) => {
  const { id, status } = request.body
	const condition = { where: { id } }

  try {
    await db.booking.update({ status }, condition)
    const booking = await db.booking.findOne(condition)
    const user = await booking.getUser()
    sendSes(user.email, id, status)
    return response.status(200).send()
  } catch (e) {
    const message = 'Unable to update booking/make payment'
		errorResponse(e, message, response)
  }
})

module.exports = router
