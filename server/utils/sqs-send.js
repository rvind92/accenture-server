const AWS = require('../setup/setup-aws')

const sqs = new AWS.SQS({ apiVersion: '2012-11-05', region: 'us-east-2' })
const QUEUE_URL = 'https://sqs.us-east-2.amazonaws.com/723249754734/accenture-worker-queue'

const send = async (message) => {
  const params = {
    DelaySeconds: 0,
    MessageBody: JSON.stringify(message),
    QueueUrl: QUEUE_URL
  }

  const sendMessage = sqs.sendMessage(params).promise()

  sendMessage
    .then(data => console.log('Data -> ', data))
    .catch(err => console.log({ error: err, message: 'Unable to send message to queue' }))
}

module.exports = send
