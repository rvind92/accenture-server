import { SELECT_HOTEL } from '../actions'

export default function(state = {}, action) {
  switch (action.type) {
    case SELECT_HOTEL:
      return action.payload
    default:
      return state
  }
}
