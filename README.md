# Application server - Accenture Assignment

This application is responsible for serving the front-end (React), authenticating users, display the list of hotels according the users profile and needs. The application also communicates with the Worker application through the SQS message queue.

The solution is deployed on AWS Elastic Beanstalk in the NodeJS environment.

## Stack
- NodeJS + ExpressJS
- AWS Elastic Beanstalk
- Amazon RDS (MySQL) / SQLite (local)

## Running the application

1. Clone or download the repository, navigate into it and run the npm install command
```
git clone
cd accenture-server
npm install
```
2. To start the application, run the following command
```
npm run start-local
```
