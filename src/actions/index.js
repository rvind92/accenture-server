import axios from 'axios'

export const SELECT_HOTEL = 'select_hotel'
export const CREATE_BOOKING = 'create_booking'
export const GET_BOOKINGS = 'get_bookings'

export const selectHotel = hotel => {
  return {
    type: SELECT_HOTEL,
    payload: hotel
  }
}

export const createBooking = booking => {
  const request = axios.post('api/booking', booking)

  return {
    type: CREATE_BOOKING,
    payload: request
  }
}

export const getBookings = () => {
  const request = axios.get('api/booking')

  return {
    type: GET_BOOKINGS,
    payload: request
  }
}
