import { GET_BOOKINGS, CREATE_BOOKING } from '../actions'

export default function(state = [], action) {
  switch (action.type) {
    case GET_BOOKINGS:
      return action.payload.data
    case CREATE_BOOKING:
      return [...state, action.payload.data]
    default:
      return state
  }
}
