import { createStore, combineReducers } from 'redux'
import HotelReducer from './hotel_reducer'
import BookingsReducer from './bookings_reducer'

const rootReducer = combineReducers({
  hotel: HotelReducer,
  bookings: BookingsReducer,
})

export default rootReducer
