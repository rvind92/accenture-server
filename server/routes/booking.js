const express = require('express')
const router = express.Router()
const db = require('../setup/setup-db')
const sendSqs = require('../utils/sqs-send')
const errorResponse = require('../utils/error-handler')

router.get('/', async (request, response) => {
  const { email } = request.user[0]
  try {
    const bookings = await db.booking.findAll({ where: { email } })
    return response.status(200).json(bookings)
  } catch (e) {
    const message = 'Unable to retrieve booking'
		errorResponse(e, message, response)
  }
})

router.post('/', async (request, response) => {
  const { card, package, pax, hotel } = request.body
  const details = { card, package, pax, hotel, email: request.user[0].email }

  const bookingPromise = new Promise(async(resolve, reject) => {
    const booking = await db.booking.create(details)
    resolve(booking)
  })

  const userPromise = new Promise(async(resolve, reject) => {
    const user = await db.user.findOne({ where: { email: request.user[0].email } })
    resolve(user)
  })

  try {
    const promises = await Promise.all([ bookingPromise, userPromise ])
    await promises[1].addBooking(promises[0])
    // send payment details to queue

    sendSqs({ booking: promises[0] })

    return response.status(200).json(promises[0])
  } catch (e) {
    const message = 'Unable to create booking/update user'
    errorResponse(e, message, response)
  }
})

module.exports = router
