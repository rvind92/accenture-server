import React, { Component } from 'react'
import axios from 'axios'
import FaBeer from 'react-icons/lib/fa/beer'
import GoogleMapReact from 'google-map-react'

const Marker = ({ index, text, vicinity, check, update }) => (
  <div>
    {check(index) && <div className='card' style={{width: '18rem', zIndex: '999'}}>
      <img className='card-img-top' src={`https://source.unsplash.com/1600x900/?hotel${index}`} alt='Card image cap' />
      <div className='card-body'>
        <h5 className='card-title'>{text}</h5>
        <p className='card-text'>{vicinity}</p>
      </div>
    </div>}
    <div
      style={{
        display: check(index) ? 'none' : 'inline',
        width: '30px',
        height: '30px',
        border: '5px solid #F18972',
        borderRadius: '30px',
        backgroundColor: 'red',
        position: 'absolute'
      }}
      onMouseOver={(e) => {
        e.preventDefault()
        update(index)
      }}
      onMouseOut={(e) => {
        e.preventDefault()
        update(index)
      }}
    >
    </div>
  </div>
)

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 13
  }

  state = {
    places: [],
    info: {}
  }

  componentDidMount() {
    this.generateMarkers()
  }

  updateState = key => {
    this.setState((state) => {
      const { info } = state
      info[key] = !info[key]
      return { info }
    })
  }

  checkInfo = key => {
    return this.state.info[key]
  }

  generateMarkers = async () => {
    try {
      const { data: { results } } = await axios.post('api/places', { lat: this.props.location.lat, lng: this.props.location.lon })
      const info = {}
      const places = results.map(({ geometry, name, vicinity }, index) => {
        info[index] = false
        return (
          <Marker
            check={this.checkInfo}
            update={this.updateState}
            index={index}
            key={index}
            lat={geometry.location.lat}
            lng={geometry.location.lng}
            text={name}
            vicinity={vicinity}
          />
        )
      })

      this.setState({ info, places })
    } catch (e) {
      const err = { error: e, message: 'Unable to fetch places' }
      console.error(err)
    }
  }

  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCT2ZLMkZOHk_Vy8PR4w-7-Im-Z2t_Fi6E' }}
          defaultCenter={{
            lat: this.props.location.lat,
            lng: this.props.location.lon
          }}
          defaultZoom={this.props.zoom}
        >
          {this.state.places}
        </GoogleMapReact>
      </div>
    )
  }
}

export default Map
