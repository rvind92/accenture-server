const Sequelize = require('sequelize')
const ENV = process.env.NODE_ENV || 'local'
const config = require('../config/config')[ENV]

let sequelize

if(ENV === 'local') { // for ease of use, I have substituted mysql with sqlite for local development
  sequelize = new Sequelize({
    dialect: 'sqlite',
    storage:  __dirname + '/../../data/accenture-local.sqlite'
  })
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host, dialect: config.dialect
  })
}

const db = {}

db.user = sequelize.import(__dirname + '/../models/user.js')
db.booking = sequelize.import(__dirname + '/../models/booking.js')
db.sequelize = sequelize
db.Sequelize = Sequelize

db.booking.belongsTo(db.user)
db.user.hasMany(db.booking)

module.exports = db
