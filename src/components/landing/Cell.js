import React, { Component } from 'react'

class Cell extends Component {
  selectHotel = e => {
    e.preventDefault()
    this.props.select(this.props.hotel)
    this.props.navigate.push('/hotel')
  }
  render() {
    return (
      <div className='col-md-4'>
        <div className='card mb-4 box-shadow'>
          <img className='card-img-top' src={this.props.image} alt='Card image cap' />
          <div className='card-body'>
            <h3>{this.props.hotel.name}</h3>
            <p className='card-text'>Hotel description text and description goes here</p>
            <div className='d-flex justify-content-between align-items-center'>
              <div className='btn-group'>
                <button onClick={this.selectHotel} type='button' className='btn btn-sm btn-outline-secondary'>View</button>
                <button type='button' className='btn btn-sm btn-outline-secondary'>Book</button>
              </div>
              <small className='text-muted'>${this.props.hotel.price}/night</small>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Cell
