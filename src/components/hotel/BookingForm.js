import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createBooking } from '../../actions'

class BookingForm extends Component {
  state = {
    card: '1234',
    package: 'Deluxe',
    pax: '1',
    error: false,
    bookings: []
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return { bookings: nextProps.bookings }
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.bookings.length > prevProps.bookings.length) {
      this.props.close()
    }
  }

  handleChange = e => {
    this.setState({ [e.target.name] : e.target.value})
  }

  handleSubmit = async (e) => {
    e.preventDefault()
    if(this.state.error) this.setState({ error: false })
    this.props.createBooking({ ...this.state, hotel: this.props.hotel.name })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>Booking</label>
        <div className='form-group'>
          <label htmlFor='card'>Card</label>
          <input name='card' type='text' className='form-control' placeholder='Enter card number' onChange={this.handleChange} />
        </div>
        <div class='form-group'>
          <label htmlFor='package'>Package</label>
          <select name='package' class='form-control' onChange={this.handleChange}>
            <option value='Deluxe'>Deluxe</option>
            <option value='Suite'>Suite</option>
            <option value='Twin Suite'>Twin Suite</option>
            <option value='Luxury'>Luxury</option>
            <option value='Penthouse'>Penthouse</option>
          </select>
        </div>
        <div class='form-group'>
          <label htmlFor='pax'>Pax</label>
          <select name='pax' class='form-control' onChange={this.handleChange}>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
            <option value='5'>5</option>
          </select>
        </div>
        {this.state.error && <div class='alert alert-danger' role='alert'>
          Unable to make booking
        </div>}
        <button type='submit' className='btn btn-primary booking-button'>Submit</button>
        <button className='btn btn-danger booking-button' onClick={e => this.props.close()}>Close</button>
      </form>
    )
  }
}

const mapStateToProps = ({ bookings, hotel }) => ({ bookings, hotel })

export default connect(mapStateToProps, { createBooking })(BookingForm)
