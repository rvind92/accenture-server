import React, { Component } from 'react'
import axios from 'axios'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import './App.css'
import Header from './components/landing/Header'
import LandingPage from './components/landing/Main'
import HotelPage from './components/hotel/Main'
import BookingsPage from './components/bookings/Main'

class App extends Component {
  state = {
    name: '',
    description: '',
    list: [],
    orders: []
  }

  render() {
    return (
      <div>
        <Router>
          <div>
            <Header />
            <Route path='/' exact component={LandingPage} />
            <Route path='/hotel' component={HotelPage} />
            <Route path='/bookings' component={BookingsPage} />
          </div>
        </Router>
      </div>
    )
  }
}

export default App
