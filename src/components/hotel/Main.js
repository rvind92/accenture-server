import React, { Component } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import Map from '../map/Map'
import BookingForm from './BookingForm'
import { createBooking } from '../../actions'
import faker from 'faker'

const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

class Hotel extends Component {
  state = {
    modalIsOpen: false
  }

  openModal = () => {
    this.setState({ modalIsOpen: true })
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false })
  }

  render() {
    return (
      <div className='container-fluid'>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel='Hotel Details'
        >
          <BookingForm close={this.closeModal} />
        </Modal>
        <div className='row' style={{ margin: '15px' }}>
          <div className='col-sm-8 col-md-8	col-lg-8'>
            <Map location={this.props.hotel.coordinates}/>
          </div>
          <div className='col-sm-4 col-md-4	col-lg-4'>
            <div className='row'>
              <button type="button" class="btn btn-primary btn-lg btn-block" onClick={this.openModal}>Make Booking</button>
              <button type="button" class="btn btn-secondary btn-lg btn-block">Contact Hotel</button>
              <button type="button" class="btn btn-success btn-lg btn-block">Favourite</button>
            </div>
            <div className='row' style={{ marginTop: '20px' }}>
              <h3>{this.props.hotel.name}</h3>
              <p>{faker.lorem.paragraphs()}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ hotel }) => ({ hotel })

export default connect(mapStateToProps, { createBooking })(Hotel)
