import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { selectHotel } from '../../actions'
import faker from 'faker'
import Cell from './Cell'

class Body extends Component {
  state = {
    cells: Array(7).fill(<Cell hotel={'https://media.giphy.com/media/s01LZudJrl3lS/giphy.gif'} />),
    modalIsOpen: false
  }

  componentDidMount() {
    this.renderHotels()
  }

  renderHotels = () => {
    const hotelsData = [
      {
        name: 'Universal City',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'hotel',
        coordinates: {
          lat: 34.145477,
          lon: -118.367632
        }
      },
      {
        name: 'Rosemead',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'hotels',
        coordinates: {
          lat: 34.051198,
          lon: -118.079167
        }
      },
      {
        name: 'Marina Del Rey',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'chalet',
        coordinates: {
          lat: 33.990723,
          lon: -118.449340
        }
      },
      {
        name: 'Redondo Beach',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'lodge',
        coordinates: {
          lat: 33.892858,
          lon: -118.363605
        }
      },
      {
        name: 'Norwalk',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'motel',
        coordinates: {
          lat: 33.912295,
          lon: -118.070046
        }
      },
      {
        name: 'Checkers',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'inn',
        coordinates: {
          lat: 34.050386,
          lon: -118.252990
        }
      },
      {
        name: 'Commerce',
        price: faker.random.number({ 'min': 50, 'max': 999 }),
        keyword: 'resort',
        coordinates: {
          lat: 34.004131,
          lon: -118.150085
        }
      }
    ]
    const hotels = []
    for(let i = 0; i < hotelsData.length; i++) {
      hotels.push(<Cell
        hotel={hotelsData[i]}
        image={`https://source.unsplash.com/1600x900/?${hotelsData[i].keyword}`}
        navigate={this.props.history}
        view={this.openModal}
        select={this.props.selectHotel}
        close={this.closeModalHandler}
      />)
    }

    this.setState({ cells: hotels })
  }

  render() {
    return (
      <main role='main'>
        <section className='jumbotron text-center'>
          <div className='container'>
            <h1 className='jumbotron-heading'>Hari Raya Promotion!</h1>
            <p className='lead text-muted'>Come and celebrate your Hari Raya getaway with these amazing deals! Promotion ends on July 1st.</p>
            <p>
              <button className='btn btn-primary my-2' style={{ margin: '5px' }}>View Offer</button>
              <button className='btn btn-secondary my-2' style={{ margin: '5px' }}>Browse Offers</button>
            </p>
          </div>
        </section>

        <div className='album py-5 bg-light'>
          <div className='container'>
            <div className='row'>
              {this.state.cells}
            </div>
          </div>
        </div>
      </main>
    )
  }
}

const mapStateToProps = ({ hotel, bookings }) => ({ hotel, bookings })

export default withRouter(connect(mapStateToProps, { selectHotel })(Body))
