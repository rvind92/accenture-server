import React, { Component } from 'react'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

class Main extends Component {
  render() {
    return (
      <div>
        <div>
          <Body />
          <Footer />
        </div>
      </div>
    )
  }
}

export default Main
