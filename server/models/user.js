module.exports = (sequelize, DataTypes) => {
	return sequelize.define('user', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    displayName: {
      type: DataTypes.STRING,
      allowNull: false
    },
		accessToken: {
			type: DataTypes.STRING,
			allowNull: false
		},
		email: {
			type: DataTypes.STRING,
			allowNull: false
		}
	})
}

/*
https://maps.googleapis.com/maps
/api/place/findplacefromtext/json?
input=${searchText}&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry

https://maps.googleapis.com/maps
/api/place/nearbysearch/json?
location=-33.8670522,151.1957362&radius=1500&type=restaurant&keyword=cruise&key=
*/
