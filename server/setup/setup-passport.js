const passport = require('passport')
const db = require('./setup-db')
const appId = process.env.FB_APP_ID || '586352288552119'
const appSecret = process.env.FB_APP_SECRET || '3bbfe46ad8dc22282d62279c17a9f3dd'
const URL = process.env.URL || 'localhost:3000'
const FacebookStrategy = require('passport-facebook').Strategy

module.exports = (passport) => {
  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((user, done) => {
    done(null, user)
  })

  passport.use(new FacebookStrategy({
    clientID: appId,
    clientSecret: appSecret,
    callbackURL: `http://${URL}/auth/facebook/callback`,
    profileFields: ['id', 'displayName', 'emails', 'name']
  }, async (accessToken, refreshToken, profile, done) => {
    const { id, displayName, email, emails } = profile
    const options = { where: { id }, defaults: { displayName, accessToken, email: emails[0].value } }

    try {
      const user = await db.user.findOrCreate(options)
      done(null, user)
    } catch (e) {
      done(e, null)
    }
  }))
}
